package com.accenture.PruebaAppiumSerenityGradle.ui;

import org.openqa.selenium.WebElement;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;

public class CalculadoraUI extends PageObject {
	
	@FindBy(id = "com.android.calculator2:id/digit2")
	private WebElement digito2;
	
	@FindBy(id = "com.android.calculator2:id/plus")
	private WebElement plus;
	
	@FindBy(id = "com.android.calculator2:id/digit8")
	private WebElement digito8;
	
	@FindBy(id = "com.android.calculator2:id/equal")
	private WebElement result;

	public void prueba() {
		digito2.click();
		plus.click();
		digito8.click();
		result.click();
	}

}