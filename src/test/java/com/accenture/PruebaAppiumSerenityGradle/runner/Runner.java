package com.accenture.PruebaAppiumSerenityGradle.runner;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@CucumberOptions(
		features = "src/test/resources/features" ,
		glue = {"com.accenture.PruebaAppiumSerenityGradle.stepDefinitions" })
@RunWith(CucumberWithSerenity.class)
public class Runner {
	
}
