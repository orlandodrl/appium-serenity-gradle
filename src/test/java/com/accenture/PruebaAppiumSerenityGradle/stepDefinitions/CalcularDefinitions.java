package com.accenture.PruebaAppiumSerenityGradle.stepDefinitions;

import org.openqa.selenium.WebDriver;

import com.accenture.PruebaAppiumSerenityGradle.steps.CalculadoraSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;

public class CalcularDefinitions {
	
	@Managed(driver = "appium")
	public WebDriver driver;
	
	@Steps
	CalculadoraSteps calculadoraSteps;

	@Given("^I want to write a step with precondition$")
	public void i_want_to_write_a_step_with_precondition() {
	    System.out.println("GIVEN");
	    calculadoraSteps.OpenPage();
	}

	@When("^I complete action$")
	public void i_complete_action() {
	    System.out.println("WHEN");
	    calculadoraSteps.prueba();
	}

	@Then("^I validate the outcomes$")
	public void i_validate_the_outcomes() {
	    System.out.println("THEN");
	}

}
